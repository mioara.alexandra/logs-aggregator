APP = logs-aggregator

PYTHON = python3

MANAGE_PY = $(PYTHON) manage.py

DOCKER_COMPOSE = docker-compose

POETRY = poetry


default: usage

usage:
	@echo -e 'Usage\n=====\n'
	@echo 'To manage your local environment, from your host use the following commands:'
	@echo ''
	@echo '- make build       	        build the app docker image'
	@echo '- make docker-start          build and update local environment'
	@echo '- make pre-commit            install pre-commit hooks'


pre-commit:
	$(POETRY) run pre-commit install

build:
	$(DOCKER_COMPOSE) -f local.yml build

docker-start:
	$(DOCKER_COMPOSE) -f local.yml up

docker-makemigrations:
	$(DOCKER_COMPOSE) -f local.yml run --rm django python manage.py makemigrations

docker-migrate:
	$(DOCKER_COMPOSE) -f local.yml run --rm django python manage.py migrate

.PHONY:						\
	pre-commit		        \
    build                   \
    docker-start            \
    docker-makemigrations   \
    docker-migrate          \
