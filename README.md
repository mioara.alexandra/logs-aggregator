# Log Aggregator Lite


This is a very lite proof of concept log aggregator service. It allows users to push and retrieve logs from a central
location. Logs are retrieved based on a filters that should provide a decent about of robustness to your queries.


## Prerequisites

In order to successfully run Log Aggregator, the following prerequisites need to be present on your machine:

- Docker; find installation instructions [here](https://docs.docker.com/get-docker/);
- Docker Compose; find installation instructions [here](https://docs.docker.com/compose/install/).

## How to run this project

Log Aggregator uses Makefile to provide easy access to the most common commands you need to use.

```sh
# Build the Docker environment
make build
# Run the Docker environment
make docker-start
```

For more commands refer directly to the Makefile.


## Accessing the local server

Once the Docker containers are running, the Log Aggregator API can be accessed on [http://localhost:8000]((http://localhost:30031)).


## API documentation
_Oops! It seems like the OpenAPI specs are not ready. Check back in a few, or enjoy this ad-hoc version._

### Users

POST: `/users/register/`
- Registers a new user.
- Requires the following payload:
```
 "password": str
 "email": str
 "username": str
 "first_name": Optional[str]
 "last_name": Optional[str]

```
- Returns the following payload:
```
 "id": int
 "username": str
 "email": str
 "first_name": str|null
 "last_name": str|null
 "is_active": boolean
 "token": str
```

### Logs

POST: `v1/logs/`

- Adds new log.
- Requires the following payload:
```
"user": int
"level": str[critical|warning|info]
"message": str
"extra_data": dict|null
```
- Returns the following payload:
```
 "id": uuid
 "user": int
 "level": str
 "message": str
 "extra_data": dict|null
```

GET: `v1/logs/`
- Retrieved a list of logs.
- Takes the following query parameters:
```
"from_time": iso formated datetime str
"to_time": iso formated datetime str
"level": str[critical|warning|info]
"message": str
```
- Returns a list of objects to the following shape:
```
 "id": uuid
 "user": int
 "level": str
 "timestamp": iso formated datetime str
 "message": str
 "extra_data": dict|null
```


## Project Maintainers

[Mioara Mihai](mailto:mioara.alexandra@gmail.com)

