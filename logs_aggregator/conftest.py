import pytest

from logs_aggregator.logs.models import Log
from logs_aggregator.users.models import User
from logs_aggregator.users.tests.factories import UserFactory


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> User:
    return UserFactory()


@pytest.fixture
def client():
    from django.test import Client

    return Client()


@pytest.fixture
def log_info(user):
    return Log.objects.create(user=user.pk, level="info", message="Dragons are hot", extra_data={"source": "internet"})


@pytest.fixture
def log_warning(user):
    return Log.objects.create(user=user.pk, level="warning", message="Here be dragons!", extra_data={"source": "fire"})


@pytest.fixture
def log_critical(user):
    return Log.objects.create(
        user=user.pk, level="critical", message="Our server took a critical hit!", extra_data={"source": "production"}
    )
