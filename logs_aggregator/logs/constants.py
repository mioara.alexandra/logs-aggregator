from enum import Enum


class LogLevel(Enum):
    INFO = "info"
    WARNING = "warning"
    CRITICAL = "critical"

    @classmethod
    def as_choices(cls):
        return [(option.value, option.value) for option in cls]
