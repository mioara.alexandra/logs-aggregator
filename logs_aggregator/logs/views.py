from rest_framework import mixins, permissions, status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from logs_aggregator.logs.models import Log
from logs_aggregator.logs.serializers import LogFiltersSerializerV1, LogSerializerV1
from logs_aggregator.logs.throttling import SafeMethodsThrottle, UnsafeMethodsThrottle


class LogsAPI(mixins.CreateModelMixin, mixins.ListModelMixin, GenericViewSet):
    queryset = Log.objects.none()
    throttle_classes = (
        SafeMethodsThrottle,
        UnsafeMethodsThrottle,
    )
    permission_classes = (permissions.IsAuthenticated,)

    def get_serializer_class(self):
        versioned_serializers = {
            "v1": LogSerializerV1,
        }
        return versioned_serializers.get(self.request.version, LogSerializerV1)

    def get_filters_serializer_class(self):
        versioned_serializers = {
            "v1": LogFiltersSerializerV1,
        }
        return versioned_serializers.get(self.request.version, LogFiltersSerializerV1)

    def get_queryset(self):
        return Log.objects.filter(user=self.request.user.pk)

    def filter_queryset(self, queryset):
        """
        Custom filtering method for logs. Multiple filters can be used:
            from_time: Get all logs since the provided datetime
            to_time: Get all logs from the provided datetime
            level: Get all logs of the provided severity level
            message = Get all logs containing (parts of) the provided message

        Returns:
            Filtered queryset object.
        """
        queryset = super().filter_queryset(queryset)

        queryset_serializer_class = self.get_filters_serializer_class()
        queryset_serializer = queryset_serializer_class(data=self.request.query_params)
        queryset_serializer.is_valid(raise_exception=True)
        validated_filters = queryset_serializer.validated_data

        queryset = self.get_queryset()
        queryset_filters = {}

        if "from_time" in validated_filters:
            queryset_filters["timestamp__gte"] = validated_filters["from_time"]
        if "to_time" in validated_filters:
            queryset_filters["timestamp__lte"] = validated_filters["to_time"]
        if "level" in validated_filters:
            queryset_filters["level"] = validated_filters["level"]
        if "message" in validated_filters:
            queryset_filters["message__icontains"] = validated_filters["message"]

        return queryset.filter(**queryset_filters)

    def create(self, request, *args, **kwargs):
        """
        Overwrite the create method to allow for both single and bulk creation of logs.

        Returns:
            A Response object with a list of created objects.
        """
        data = request.data
        if isinstance(data, dict):
            data = [data]

        serializer = self.get_serializer(data=data, many=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
