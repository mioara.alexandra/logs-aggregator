from rest_framework import permissions
from rest_framework.throttling import UserRateThrottle


class SafeMethodsThrottle(UserRateThrottle):
    scope = "safe"

    def allow_request(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return super().allow_request(request, view)
        return True


class UnsafeMethodsThrottle(UserRateThrottle):
    scope = "unsafe"

    def allow_request(self, request, view):
        if request.method not in permissions.SAFE_METHODS:
            return super().allow_request(request, view)
        return True
