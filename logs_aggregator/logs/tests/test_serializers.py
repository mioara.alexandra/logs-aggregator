from logs_aggregator.logs.models import Log
from logs_aggregator.logs.serializers import LogSerializerV1


def test_log_serializer_v1(log_info):
    payload = LogSerializerV1(instance=log_info)
    assert payload.data == {
        "id": log_info.pk,
        "user": log_info.user.pk,
        "level": "info",
        "timestamp": log_info.timestamp.isoformat(),
        "message": "Tabs are better than spaces",
        "extra_data": {"source": "internet"},
    }


def test_write_log(user):
    data = {
        "user": user.pk,
        "level": "critical",
        "message": "Our server took a critical hit!",
        "extra_data": {"service": "test"},
    }
    payload = LogSerializerV1(data=data)
    payload.is_valid(raise_exception=True)
    payload.save()

    new_log = Log.objects.last()
    assert new_log.timestamp
    assert new_log.message == "Our server took a critical hit!"
    assert new_log.user == user
