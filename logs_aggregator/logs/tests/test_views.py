from urllib.parse import urlencode

from django.urls import reverse
from django.utils import timezone


def test_create_logs_view__single(client, user):
    data = {
        "user": user.pk,
        "level": "critical",
        "message": "Our server took a critical hit!",
        "extra_data": {"service": "test"},
    }

    resp = client.post(reverse("logs:logs-detail"), data)
    assert resp.status_code == 201
    assert isinstance(resp.data, list)


def test_create_logs_view__bulk(client, user):
    data = [
        {"user": user.pk, "level": "info", "message": "Tabs are better than spaces", "extra_data": {"service": "test"}},
        {"user": user.pk, "level": "warning", "message": "You have been warned", "extra_data": {"service": "test"}},
    ]

    resp = client.post(reverse("logs:logs-detail"), data)
    assert resp.status_code == 201
    assert isinstance(resp.data, list)


def test_list_logs_view__no_filters(client, log_info, log_warning, log_critical):
    # When querying logs without providing filters...
    resp = client.get(reverse("logs:logs-list"))
    assert resp.status_code == 200
    # ...all logs belonging to that user are returned.
    assert len(resp) == 3


def test_list_logs_view__from_time(client, log_info, log_warning, log_critical):
    # When querying logs with a from_time filter after the time of creation...
    query_parameters = {
        "from_time": timezone.now().isoformat(),
    }
    resp = client.get(f'{reverse("logs:logs-list")}?{urlencode(query_parameters)}')
    assert resp.status_code == 200
    # ...no logs are returned.
    assert len(resp) == 0


def test_list_logs_view__to_time(client, log_info, log_warning, log_critical):
    # When querying logs with a to_time filter...
    query_parameters = {
        "to_time": log_info.timestamp.isoformat(),
    }
    resp = client.get(f'{reverse("logs:logs-list")}?{urlencode(query_parameters)}')
    assert resp.status_code == 200
    # ...only logs created at most at that time are returned.
    assert len(resp) == 1
    assert len[0] == log_info


def test_list_logs_view__level(client, log_info, log_warning, log_critical):
    # When querying logs with a level filter...
    query_parameters = {
        "level": "warning",
    }
    resp = client.get(f'{reverse("logs:logs-list")}?{urlencode(query_parameters)}')
    assert resp.status_code == 200
    # ...only logs of that level are returned.
    assert len(resp) == 1
    assert resp[0] == log_warning


def test_list_logs_view__message(client, log_info, log_warning, log_critical):
    # When querying logs with a message filter...
    query_parameters = {
        "message": "dragons",
    }
    resp = client.get(f'{reverse("logs:logs-list")}?{urlencode(query_parameters)}')
    assert resp.status_code == 200
    # ...only logs which (partially) include the provided message are returned.
    assert len(resp) == 2
    assert "dragons" in resp[0].message
    assert "dragons" in resp[1].message
