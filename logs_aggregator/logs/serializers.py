from rest_framework import serializers

from logs_aggregator.logs.models import Log


class LogSerializerV1(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)
    timestamp = serializers.ReadOnlyField()

    class Meta:
        model = Log
        fields = [
            "id",
            "user",
            "level",
            "timestamp",
            "message",
            "extra_data",
        ]


class LogFiltersSerializerV1(serializers.Serializer):
    from_time = serializers.DateTimeField(required=False)
    to_time = serializers.DateTimeField(required=False)
    level = serializers.CharField(required=False)
    message = serializers.CharField(required=False)
