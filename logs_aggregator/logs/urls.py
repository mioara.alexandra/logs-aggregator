from django.urls import include, path
from rest_framework.routers import DefaultRouter

from logs_aggregator.logs import views


router = DefaultRouter()
router.register(r"logs", views.LogsAPI)

app_name = "logs"
urlpatterns = [
    path("", include(router.urls)),
]
