import uuid

from django.contrib.postgres.fields import JSONField
from django.db import models

from logs_aggregator.logs.constants import LogLevel


class Log(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey("users.User", on_delete=models.CASCADE, related_name="logs")
    timestamp = models.DateTimeField(auto_now_add=True)
    level = models.CharField(max_length=22, choices=LogLevel.as_choices(), default=LogLevel.WARNING.value)
    message = models.CharField(max_length=255, help_text="Description of the incident. Can be used for aggregation.")
    extra_data = JSONField()

    class Meta:
        verbose_name = "logs"
